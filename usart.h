#ifndef __USART_H__
#define __USART_H__

#include <stdint.h>

void USART1_Init(void);
int USART1_GetKey(void);
extern void  ttywrch (int ch);
extern int stderr_putchar (int ch);
extern int stdin_getchar (void);
extern int stdout_putchar (int ch);
char* USART1_GetRx(void);
void USART1_Test(void);

#endif // __USART_H__
