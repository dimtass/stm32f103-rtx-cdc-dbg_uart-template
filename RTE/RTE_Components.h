
/*
 * Auto generated Run-Time-Environment Component Configuration File
 *      *** Do not modify ! ***
 *
 * Project: 'stm32_uart' 
 * Target:  'Target 1' 
 */

#ifndef RTE_COMPONENTS_H
#define RTE_COMPONENTS_H


/*
 * Define the Device Header File: 
 */
#define CMSIS_device_header "stm32f10x.h"

#define RTE_CMSIS_RTOS                  /* CMSIS-RTOS */
        #define RTE_CMSIS_RTOS_RTX              /* CMSIS-RTOS Keil RTX */
#define RTE_Compiler_IO_STDERR          /* Compiler I/O: STDERR */
        #define RTE_Compiler_IO_STDERR_User     /* Compiler I/O: STDERR User */
#define RTE_Compiler_IO_STDIN           /* Compiler I/O: STDIN */
        #define RTE_Compiler_IO_STDIN_User      /* Compiler I/O: STDIN User */
#define RTE_Compiler_IO_STDOUT          /* Compiler I/O: STDOUT */
        #define RTE_Compiler_IO_STDOUT_User     /* Compiler I/O: STDOUT User */
#define RTE_Compiler_IO_TTY             /* Compiler I/O: TTY */
        #define RTE_Compiler_IO_TTY_User        /* Compiler I/O: TTY User */
#define RTE_DEVICE_STDPERIPH_FRAMEWORK
#define RTE_DEVICE_STDPERIPH_GPIO
#define RTE_DEVICE_STDPERIPH_RCC
#define RTE_DEVICE_STDPERIPH_USART
#define RTE_Drivers_USBD0               /* Driver USBD0 */
#define RTE_USB_Core                    /* USB Core */
#define RTE_USB_Device_0                /* USB Device 0 */
#define RTE_USB_Device_CDC_0            /* USB Device CDC instance 0 */

#endif /* RTE_COMPONENTS_H */
