/*----------------------------------------------------------------------------
 * Name:    Usart.c
 * Purpose: USART usage for STM32
 * Version: V1.00
 *----------------------------------------------------------------------------
 * This file is part of the uVision/ARM development tools.
 * This software may only be used under the terms of a valid, current,
 * end user licence from KEIL for a compatible version of KEIL software
 * development tools. Nothing else gives you the right to use this software.
 *
 * Copyright (c) 2005-2007 Keil Software. All rights reserved.
 *----------------------------------------------------------------------------*/

#include <stdio.h>
#include "stm32f10x.h"		// STM32F10x Library Definitions

#define USART_FLAG_CTS                       ((uint16_t)0x0200)
#define USART_FLAG_LBD                       ((uint16_t)0x0100)
#define USART_FLAG_TXE                       ((uint16_t)0x0080)
#define USART_FLAG_TC                        ((uint16_t)0x0040)
#define USART_FLAG_RXNE                      ((uint16_t)0x0020)
#define USART_FLAG_IDLE                      ((uint16_t)0x0010)
#define USART_FLAG_ORE                       ((uint16_t)0x0008)
#define USART_FLAG_NE                        ((uint16_t)0x0004)
#define USART_FLAG_FE                        ((uint16_t)0x0002)
#define USART_FLAG_PE                        ((uint16_t)0x0001)
#define IS_USART_FLAG(FLAG) (((FLAG) == USART_FLAG_PE) || ((FLAG) == USART_FLAG_TXE) || \
                             ((FLAG) == USART_FLAG_TC) || ((FLAG) == USART_FLAG_RXNE) || \
                             ((FLAG) == USART_FLAG_IDLE) || ((FLAG) == USART_FLAG_LBD) || \
                             ((FLAG) == USART_FLAG_CTS) || ((FLAG) == USART_FLAG_ORE) || \
                             ((FLAG) == USART_FLAG_NE) || ((FLAG) == USART_FLAG_FE))

#define IS_USART_CLEAR_FLAG(FLAG) ((((FLAG) & (uint16_t)0xFC9F) == 0x00) && ((FLAG) != (uint16_t)0x00))
#define IS_USART_PERIPH_FLAG(PERIPH, USART_FLAG) ((((*(uint32_t*)&(PERIPH)) != UART4_BASE) &&\
                                                  ((*(uint32_t*)&(PERIPH)) != UART5_BASE)) \
                                                  || ((USART_FLAG) != USART_FLAG_CTS))
#define IS_USART_BAUDRATE(BAUDRATE) (((BAUDRATE) > 0) && ((BAUDRATE) < 0x0044AA21))
#define IS_USART_ADDRESS(ADDRESS) ((ADDRESS) <= 0xF)
#define IS_USART_DATA(DATA) ((DATA) <= 0x1FF)

/*----------------------------------------------------------------------------
  Notes:
  The length of the receive and transmit buffers must be a power of 2.
  Each buffer has a next_in and a next_out index.
  If next_in = next_out, the buffer is empty.
  (next_in - next_out) % buffer_size = the number of characters in the buffer.
 *----------------------------------------------------------------------------*/
#define TBUF_SIZE   256	     /*** Must be a power of 2 (2,4,8,16,32,64,128,256,512,...) ***/
#define RBUF_SIZE   256      /*** Must be a power of 2 (2,4,8,16,32,64,128,256,512,...) ***/

/*----------------------------------------------------------------------------
 *----------------------------------------------------------------------------*/
#if TBUF_SIZE < 2
#error TBUF_SIZE is too small.  It must be larger than 1.
#elif ((TBUF_SIZE & (TBUF_SIZE-1)) != 0)
#error TBUF_SIZE must be a power of 2.
#endif

#if RBUF_SIZE < 2
#error RBUF_SIZE is too small.  It must be larger than 1.
#elif ((RBUF_SIZE & (RBUF_SIZE-1)) != 0)
#error RBUF_SIZE must be a power of 2.
#endif

/*----------------------------------------------------------------------------
 *----------------------------------------------------------------------------*/
struct buf_st {
    unsigned int in;                                // Next In Index
    unsigned int out;                               // Next Out Index
    char buf[RBUF_SIZE];                           // Buffer
};

static struct buf_st rbuf = { 0, 0, };
#define SIO_RBUFLEN ((unsigned short)(rbuf.in - rbuf.out))

static struct buf_st tbuf = { 0, 0, };
#define SIO_TBUFLEN ((unsigned short)(tbuf.in - tbuf.out))

static unsigned int tx_restart = 1;               // NZ if TX restart is required

//extern tp_glb glb;

/*----------------------------------------------------------------------------
  USART1_IRQHandler
  Handles USART1 global interrupt request.
 *----------------------------------------------------------------------------*/
void USART1_IRQHandler(void) {
    volatile unsigned int IIR;
    struct buf_st *p;

    IIR = USART1->SR;
    if (IIR & USART_FLAG_RXNE) {                  // read interrupt
        USART1->SR &= ~USART_FLAG_RXNE;             // clear interrupt

        p = &rbuf;

        if (((p->in - p->out) & ~(RBUF_SIZE - 1)) == 0) {
            p->buf[p->in & (RBUF_SIZE - 1)] = (USART1->DR & 0x1FF);
            p->in++;
            //glb.rx_ready = 10;
        }
    }
		

    if (IIR & USART_FLAG_TXE) {
        USART1->SR &= ~USART_FLAG_TXE;              // clear interrupt

        p = &tbuf;

        if (p->in != p->out) {
            USART1->DR = (p->buf[p->out & (TBUF_SIZE - 1)] & 0x1FF);
            p->out++;
            tx_restart = 0;
        } else {
            tx_restart = 1;
            USART1->CR1 &= ~USART_FLAG_TXE;           // disable TX interrupt if nothing to send
        }
    }
}



/*------------------------------------------------------------------------------
  buffer_Init
  initialize the buffers
 *------------------------------------------------------------------------------*/
void USART1_Init(void) {
	
	USART_InitTypeDef USART_InitStructure;

	tbuf.in = 0;                                    // Clear com buffer indexes
	tbuf.out = 0;
	tx_restart = 1;

	rbuf.in = 0;
	rbuf.out = 0;
	
	
  USART_InitStructure.USART_BaudRate = 9600;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

  /* Configure USARTy */
  USART_Init(USART1, &USART_InitStructure);
  
  /* Enable USARTy Receive and Transmit interrupts */
  USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
  USART_ITConfig(USART1, USART_IT_TXE, ENABLE);

  /* Enable the USARTy */
  USART_Cmd(USART1, ENABLE);
}


/*------------------------------------------------------------------------------
  SenChar
  transmit a character
 *------------------------------------------------------------------------------*/
void  ttywrch (int ch) {
    struct buf_st *p = &tbuf;

    // If the buffer is full, return an error value
		if (SIO_TBUFLEN >= TBUF_SIZE) return;

    p->buf[p->in & (TBUF_SIZE - 1)] = ch;           // Add data to the transmit buffer.
    p->in++;

    if (tx_restart) {                               // If transmit interrupt is disabled, enable it
        tx_restart = 0;
        USART1->CR1 |= USART_FLAG_TXE;                // enable TX interrupt
    }

    //return (0);
}

int stderr_putchar (int ch) {
	ttywrch(ch);
	return 0;
}


int stdout_putchar (int ch)
{
	ttywrch(ch);
	return 0;
}

/*------------------------------------------------------------------------------
  GetKey
  receive a character
 *------------------------------------------------------------------------------*/
int stdin_getchar(void) {
    struct buf_st *p = &rbuf;

    if (SIO_RBUFLEN == 0) return (-1);

    return (p->buf[(p->out++) & (RBUF_SIZE - 1)]);
}


char* USART1_GetRx(void) {
    rbuf.buf[rbuf.in] = 0;
    rbuf.in = 0;
    rbuf.out = 0;
    return ((char *)rbuf.buf);
}
